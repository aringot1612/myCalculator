﻿using myCalculator.Core.Interfaces.Business;
using myCalculator.Core.Interfaces.DataBaseRepository;
using myCalculator.Entities.Models;

namespace myCalculator.Business.Services
{
    public class WorkoutService : IWorkoutService
    {
        private readonly IWorkoutRepository workoutRepository;

        public WorkoutService(IWorkoutRepository workoutRepository)
        {
            this.workoutRepository = workoutRepository;
        }

        public WorkoutModel Retrieve(int id)
        {
            return workoutRepository.Select(id);
        }

        public IEnumerable<WorkoutModel> RetrieveAll()
        {
            return workoutRepository.SelectAll();
        }

        public WorkoutModel Create(WorkoutModel model)
        {
            return workoutRepository.Insert(model);
        }

        public WorkoutModel Update(int id, WorkoutModel model)
        {
            return workoutRepository.Update(id, model);
        }
    }
}
