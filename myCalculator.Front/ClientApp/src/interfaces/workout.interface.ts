export interface IWorkoutModel {
  id: number;
  date: Date;
  duration: number;
}
