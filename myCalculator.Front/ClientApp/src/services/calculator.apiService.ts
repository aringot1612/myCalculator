import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';

@Injectable()
export class CalculatorApiService {
  constructor(private _httpClient: HttpClient, private _apiService: ApiService) { }

    async Put(path: string, value: string[]): Promise<string> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        const requestOptions: Object = { ...headers, responseType: 'text' }
        let myData = { input: value };
        let res: string = "undefined";
        await this._apiService.Put(path, requestOptions, myData).then(r => { res = String(r) });
        return String(res);
    }
}
