import { Injectable } from '@angular/core';
import { CalculatorApiService } from '../../services/calculator.apiService';

@Injectable()
export class CalculatorService {
    constructor(private _apiService: CalculatorApiService) { }

    async PutValue(value: string[]): Promise<string> {
        return this._apiService.Put("computing", value);
    }
}
