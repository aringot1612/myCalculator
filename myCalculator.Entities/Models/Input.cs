﻿namespace myCalculator.Entities.Models
{
    public class Input
    {
        public string Value { get; set; }
        public int Id { get; set; }
    }
}