﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using myCalculator.Core.Interfaces;
using myCalculator.Entities.Models;

namespace myCalculator.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ComputingController : ControllerBase
    {
        private readonly IComputingService computingService;

        public ComputingController(IComputingService computingService)
        {
            this.computingService = computingService;
        }

        [HttpPut]
        public IActionResult Put([FromBody] Data data)
        {
            string res = computingService.Analyse(data.Input);
            return Ok(res);
        }
    }
}