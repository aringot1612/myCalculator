﻿using MathNet.Numerics;
using myCalculator.Core.Interfaces;
using myCalculator.Entities.Models;

namespace myCalculator.Business
{
    public class ComputingService : IComputingService
    {
        public string Analyse(List<string> input)
        {
            int tmpIndex = input.FindIndex(ind => ind.Equals("PI"));
            if (tmpIndex != -1)
            {
                input[tmpIndex] = Math.PI.ToString("F5");
            }

            tmpIndex = input.FindIndex(ind => ind.Equals("e"));
            if (tmpIndex != -1)
            {
                input[tmpIndex] = Math.E.ToString("F5");
            }

            List<Input> inputList = new();
            int startIndex = 0;
            int stopIndex = 0;
            int parenthesisStartIndex = -1;
            int parenthesisStopIndex = -1;
            Input calculation = new();
            do
            {
                inputList.Clear();
                startIndex = 0;
                stopIndex = 0;
                parenthesisStartIndex = input.LastIndexOf("(");
                parenthesisStopIndex = input.IndexOf(")");
                if (parenthesisStopIndex - parenthesisStartIndex == 2)
                {
                    input.RemoveAt(parenthesisStopIndex);
                    input.RemoveAt(parenthesisStartIndex);
                    parenthesisStartIndex = -1;
                    parenthesisStopIndex = -1;
                }
                if (parenthesisStartIndex != -1)
                {
                    startIndex = parenthesisStartIndex;
                }
                else
                {
                    startIndex = 0;
                }

                if (parenthesisStopIndex != -1)
                {
                    stopIndex = parenthesisStopIndex;
                }
                else
                {
                    stopIndex = input.Count;
                }

                for (int i = startIndex; i < stopIndex; i++)
                {
                    if (!double.TryParse(input[i], out double n) && input[i] != "(" && input[i] != ")")
                    {
                        inputList.Add(new Input { Id = i, Value = input[i] });
                    }
                }

                inputList = inputList.OrderByDescending(e => e.Value).ToList();
                if (inputList.Count > 0)
                {
                    calculation = inputList[0];
                    if (calculation.Value == "abs")
                    {
                        input[calculation.Id - 1] = Abs(double.Parse(input[calculation.Id - 1])).ToString("F2");
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "exp")
                    {
                        input[calculation.Id - 1] = Exp(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                        input.RemoveAt(calculation.Id + 1);
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "mod")
                    {
                        input[calculation.Id - 1] = Mod(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                        input.RemoveAt(calculation.Id + 1);
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "n!")
                    {
                        input[calculation.Id - 1] = Factorial(double.Parse(input[calculation.Id - 1])).ToString("F2");
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "/")
                    {
                        input[calculation.Id - 1] = Divide(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                        input.RemoveAt(calculation.Id + 1);
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "x")
                    {
                        input[calculation.Id - 1] = Multiply(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                        input.RemoveAt(calculation.Id + 1);
                        input.RemoveAt(calculation.Id);
                    }
                    else if (calculation.Value == "-")
                    {
                        if (calculation.Id - 1 >= 0)
                        {
                            if (input[calculation.Id - 1] == "-")
                            {
                                input[calculation.Id] = "+";
                                input.RemoveAt(calculation.Id - 1);
                            }
                            else
                            {
                                if (!double.TryParse(input[calculation.Id - 1], out double n))
                                {
                                    input[calculation.Id + 1] = (-double.Parse(input[calculation.Id + 1])).ToString("F2");
                                    input.RemoveAt(calculation.Id);
                                }
                                else
                                {
                                    input[calculation.Id - 1] = Minus(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                                    input.RemoveAt(calculation.Id + 1);
                                    input.RemoveAt(calculation.Id);
                                }
                            }
                        }
                        else
                        {
                            input[calculation.Id + 1] = (-double.Parse(input[calculation.Id + 1])).ToString("F2");
                            input.RemoveAt(calculation.Id);
                        }
                    }
                    else if (calculation.Value == "+")
                    {
                        input[calculation.Id - 1] = Add(double.Parse(input[calculation.Id - 1]), double.Parse(input[calculation.Id + 1])).ToString("F2");
                        input.RemoveAt(calculation.Id + 1);
                        input.RemoveAt(calculation.Id);
                    }
                }
                if (parenthesisStopIndex - parenthesisStartIndex == 4)
                {
                    input.RemoveAt(parenthesisStartIndex);
                    input.RemoveAt(parenthesisStartIndex + 1);
                }
            } while (input.Count > 1);
            return input[0];
        }

        private static double Abs(double value)
        {
            return Math.Abs(value);
        }

        private static double Add(double val1, double val2)
        {
            return (val1 + val2);
        }

        private static double Divide(double val1, double val2)
        {
            return (val1 / val2);
        }

        private static double Exp(double val1, double val2)
        {
            return Math.Pow(val1, val2);
        }

        private static double Factorial(double value)
        {
            return (value * SpecialFunctions.Gamma(value));
        }

        private static double Minus(double val1, double val2)
        {
            return (val1 - val2);
        }

        private static double Mod(double val1, double val2)
        {
            return (val1 % val2);
        }

        private static double Multiply(double val1, double val2)
        {
            return (val1 * val2);
        }
    }
}