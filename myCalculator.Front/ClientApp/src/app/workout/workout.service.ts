import { Injectable } from '@angular/core';
import { IWorkoutModel } from '../../interfaces/workout.interface';
import { WorkoutApiService } from '../../services/workout.apiService';

@Injectable()
export class WorkoutService {
  constructor(private _apiService: WorkoutApiService) { }

  PutValue(value: IWorkoutModel) {
    return this._apiService.Put("fitness", value);
  }

  PostValue(value: IWorkoutModel) {
    return this._apiService.Post("fitness", value);
  }

  GetValue() {
    return this._apiService.Get("fitness");
  }
}
