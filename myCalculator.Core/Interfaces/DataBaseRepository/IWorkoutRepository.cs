﻿using myCalculator.Entities.Models;

namespace myCalculator.Core.Interfaces.DataBaseRepository
{
    public interface IWorkoutRepository
    {
        WorkoutModel Select(int id);
        IEnumerable<WorkoutModel> SelectAll();
        WorkoutModel Insert(WorkoutModel model);
        WorkoutModel Update(int id, WorkoutModel model);
    }
}
