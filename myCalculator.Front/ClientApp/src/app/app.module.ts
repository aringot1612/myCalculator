import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material-module'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalculatorService } from './calculator/calculator.service';
import { WorkoutComponent } from './workout/workout.component';
import { WorkoutService } from './workout/workout.service';
import { CalculatorApiService } from '../services/calculator.apiService';
import { WorkoutApiService } from '../services/workout.apiService';
import { ApiService } from '../services/api.service';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        CalculatorComponent,
        WorkoutComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule
  ],
  providers: [CalculatorService, WorkoutService, CalculatorApiService, WorkoutApiService, ApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
