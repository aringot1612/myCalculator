﻿namespace myCalculator.Core.Interfaces
{
    public interface IComputingService
    {
        string Analyse(List<string> input);
    }
}