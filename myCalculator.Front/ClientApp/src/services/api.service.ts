import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {
    constructor(private _httpClient: HttpClient) { }

    async Get(path: string, requestOptions: Object){
        let res;
        await this._httpClient.get(environment.apiUrl + path, requestOptions).toPromise().then(data => res = data);
        return res;
    }

    async Put(path: string, requestOptions: Object, value: Object) {
        let res;
        await this._httpClient.put(environment.apiUrl + path, value, requestOptions).toPromise().then(data => res = data);
        return res;
    }

    async Post(path: string, requestOptions: Object, value: Object) {
      let res;
      await this._httpClient.post(environment.apiUrl + path, value, requestOptions).toPromise().then(data => res = data);
      return res;
    }
}
