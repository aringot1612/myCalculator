﻿using myCalculator.Entities.Models;

namespace myCalculator.Core.Interfaces.Business
{
    public interface IWorkoutService
    {
        WorkoutModel Retrieve(int id);
        IEnumerable<WorkoutModel> RetrieveAll();
        WorkoutModel Create(WorkoutModel model);
        WorkoutModel Update(int id, WorkoutModel model);
    }
}
