import { HostListener } from '@angular/core';
import { Component } from '@angular/core';
import { CalculatorService } from './calculator.service';

@Component({
    selector: 'calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {
    input: string[] = [];
    inputForPrint: string = "";
    result: string = "";

    constructor(private _calculatorService: CalculatorService) { }

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        this.controlInput(event.key);
    }

    updateInput(event: any) {
        if (typeof event.path[0].innerText === 'string') {
            this.controlInput(event.path[0].innerText);
        }
    }

    controlInput(value: string) {
        if (value === "Erase") {
            this.input = [];
        }
        else if (value === "C") {
            this.input.pop();
        }
        else if (value === "=") {
            this.sendCalculation();
        }
        else if (value === "1/x") {
            if (this.input[this.input.length - 1] == ')') {
                let i = this.input.length;
                while (i >= 0) {
                    if (this.input[i] == '(') {
                        this.input.splice(i, 0, "/");
                        this.input.splice(i, 0, "1");
                        i = -1;
                    }
                    else {
                        i--;
                    }
                }
            }
            else {
                let i = this.input.length;
                while (i >= 0) {
                    if ((this.input[i] >= '0' && this.input[i] <= '9') || this.input[i] == '.') {
                        this.input.splice(i, 0, "/");
                        this.input.splice(i, 0, "1");
                        i = -1;
                    }
                    else {
                        i--;
                    }
                }
            }
        }
        else if (value === "+/-") {
            if (this.input[this.input.length - 1] == ')') {
                let i = this.input.length;
                while (i >= 0) {
                    if (this.input[i] == '(') {
                        this.input.splice(i, 0, "-");
                        i = -1;
                    }
                    else {
                        i--;
                    }
                }
            }
            else {
                let i = this.input.length;
                while (i >= 0) {
                    if ((this.input[i] >= '0' && this.input[i] <= '9') || this.input[i] == '.') {
                        this.input[i] = [this.input[i].slice(0, 0), "-", this.input[i].slice(0)].join('');
                        i = -1;
                    }
                    else {
                        i--;
                    }
                }
            }
        }
        else if (value === ".") {
            if (this.input.length == 0) {
                this.input.push("0.");
            }
            else if (this.input[this.input.length - 1].slice(-1) >= '0' && this.input[this.input.length - 1].slice(-1) <= '9') {
                this.input[this.input.length - 1] += ".";
            }
        }
        else if (value >= '0' && value <= '9') {
            if (this.input.length != 0 && (this.input[this.input.length - 1].slice(-1) === "." || (this.input[this.input.length - 1].slice(-1) >= '0' && this.input[this.input.length - 1].slice(-1) <= '9'))) {
                this.input[this.input.length - 1] += value;
            }
            else {
                this.input.push(value);
            }
        }
        else {
            this.input.push(value);
        }
        this.inputForPrint = this.input.join("");
    }

  async sendCalculation() {
    this._calculatorService.PutValue(this.input).then(r => {this.result = r});
  }
}
