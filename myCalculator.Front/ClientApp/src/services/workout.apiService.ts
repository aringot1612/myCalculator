import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { IWorkoutModel } from '../interfaces/workout.interface';
import { empty } from 'rxjs';

@Injectable()
export class WorkoutApiService {
  constructor(private _httpClient: HttpClient, private _apiService: ApiService) { }

    Get(path: string) {
      const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
      const requestOptions: Object = { ...headers, responseType: 'json' };
      return this._apiService.Get(path, requestOptions);
    }

  Put(path: string, value: IWorkoutModel): IWorkoutModel{
      const headers = new HttpHeaders().set('Content-Type', 'application/json');
      const requestOptions: Object = { ...headers, responseType: 'json' };
      let res: IWorkoutModel = { id: 0, date: new Date(), duration: 0 };
      this._apiService.Put(path, requestOptions, value).then(r => { res = JSON.parse(String(r)) });
      return res;
    }

  Post(path: string, value: IWorkoutModel): IWorkoutModel{
      const headers = new HttpHeaders().set('Content-Type', 'application/json');
      const requestOptions: Object = { ...headers, responseType: 'text' }
      let res: IWorkoutModel = { id: 0, date: new Date(), duration: 0 };
      this._apiService.Post(path, requestOptions, value).then(r => { res = JSON.parse(String(r)) });
      return res;
    }
}
