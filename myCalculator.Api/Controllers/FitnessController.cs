﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using myCalculator.Core.Interfaces.Business;
using myCalculator.Entities.Models;

namespace myCalculator.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class FitnessController : ControllerBase
    {
        private readonly IWorkoutService workoutService;

        public FitnessController(IWorkoutService workoutService)
        {
            this.workoutService = workoutService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll()
        {
            return Ok(workoutService.RetrieveAll());
        }

        [HttpPut]
        public IActionResult Put([FromBody] WorkoutModel model)
        {
            WorkoutModel res = workoutService.Create(model);
            return Ok(res);
        }

        [HttpPost]
        public IActionResult Post([FromBody] WorkoutModel model)
        {
            WorkoutModel res = workoutService.Update(model.Id, model);
            return Ok(res);
        }
    }
}