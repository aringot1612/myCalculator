﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using myCalculator.Entities.Models;
using System.Configuration;
using System.Reflection;

namespace myCalculator.DatabaseRepository.Context
{
    public class WorkoutContext : DbContext
    {
        public WorkoutContext(DbContextOptions<WorkoutContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkoutModel>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
        }

        public DbSet<WorkoutModel>? WorkoutEntries { get; set; }
    }
}
