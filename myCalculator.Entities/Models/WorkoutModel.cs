﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace myCalculator.Entities.Models
{
    public class WorkoutModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Duration { get; set; }
    }
}
