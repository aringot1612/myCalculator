import { Component } from '@angular/core';
import { WorkoutService } from './workout.service';
import { IWorkoutModel } from '../../interfaces/workout.interface';

@Component({
    selector: 'workout',
    templateUrl: './workout.component.html',
    styleUrls: ['./workout.component.scss']
})
export class WorkoutComponent {
    sessionState: boolean = false;
    startDate: any;
    stopDate: any;
    counter: number = 0;
    currentSession: IWorkoutModel = {id: 0, date: new Date(), duration: 0 };

    constructor(private _workoutService: WorkoutService) { }

    onSessionStart(){
      this.sessionState = true;
      this.startDate = performance.now();
    }

    onSessionStop() {
      this.sessionState = false;
      this.stopDate = performance.now();
      var timeDiff = this.stopDate - this.startDate;
      timeDiff /= 1000;
      var seconds = Math.round(timeDiff);
      this.currentSession.duration = seconds;
      this.currentSession = this._workoutService.PutValue(this.currentSession);
    }

    incrementCounter() {
      this.counter++;
    }

    resetCounter() {
      this.counter = 0;
    }
}
