﻿using myCalculator.Core.Interfaces.DataBaseRepository;
using myCalculator.DatabaseRepository.Context;
using myCalculator.Entities.Models;
using System.Collections.Generic;

namespace myCalculator.DatabaseRepository.Repositories
{
    public class WorkoutRepository : IWorkoutRepository
    {
        private readonly WorkoutContext workoutContext;

        public WorkoutRepository(WorkoutContext workoutContext)
        {
            this.workoutContext = workoutContext;
        }

        public WorkoutModel Select(int id)
        {
            return workoutContext.WorkoutEntries?.Find(id) ?? new WorkoutModel { Id = -1 };
        }

        public IEnumerable<WorkoutModel> SelectAll()
        {
            IEnumerable<WorkoutModel>? results = workoutContext.WorkoutEntries;
            if (results != null)
            {
                return results;
            }
            else
            {
                return new List<WorkoutModel>(0);
            }
        }

        public WorkoutModel Insert(WorkoutModel model)
        {
            workoutContext.WorkoutEntries?.Add(model);
            workoutContext.SaveChanges();
            return model;
        }

        public WorkoutModel Update(int id, WorkoutModel model)
        {
            WorkoutModel existingWorkoutModel = Select(id);
            existingWorkoutModel.Id = existingWorkoutModel.Id;
            existingWorkoutModel.Date = existingWorkoutModel.Date;
            existingWorkoutModel.Duration = existingWorkoutModel.Duration;
            workoutContext.SaveChanges();
            return existingWorkoutModel;
        }
    }
}
